"""
The code is a minimal example of creating RB-LFR benchmarks. Most is hard-coded, and only one level is implemented. 
However, this approach can be extended to multiple levels.
"""

SEED = 10

import networkx as nx                                           # Network utilities
from networkx.generators.community import LFR_benchmark_graph   # Import method to create LFR benchmark graphs
from copy import deepcopy                                       # Lots of deepcopying is done in creating replica networks
import matplotlib.pyplot as plt                                 # Plotting



# Create an initial graph
n = 250
tau1 = 3
tau2 = 1.5
mu = 0.1
max_degree = int(0.1 * n)
G = LFR_benchmark_graph(
    n, tau1, tau2, mu, average_degree=5, min_community=20, seed=SEED, max_degree=max_degree
)

# Find community hubs in the graph, which are used to connect to the replicas
communities = {frozenset(G.nodes[v]["community"]) for v in G}
degrees = G.degree()
hubs = {}
for n, community in zip(range(len(communities)), list(communities)):
    curr_hub = None
    curr_degree = 0
    for node in list(community):
        degree = degrees[node]
        if degree > curr_degree:
            curr_hub = node
            curr_degree = degree
    hubs[n] = curr_hub

# Create a deepcopy of the graph which is used later
_G = deepcopy(G)

# Create first level of replicas
G1 = deepcopy(G)
for v in G1:
    G1.nodes[v]["community"] = {value + 250 * 1 for value in G1.nodes[v]["community"]} 
G2 = deepcopy(G)
for v in G2:
    G2.nodes[v]["community"] = {value + 250 * 2 for value in G2.nodes[v]["community"]}
G3 = deepcopy(G)
for v in G3:
    G3.nodes[v]["community"] = {value + 250 * 3 for value in G3.nodes[v]["community"]}
G4 = deepcopy(G)
for v in G4:
    G4.nodes[v]["community"] = {value + 250 * 4 for value in G4.nodes[v]["community"]}



"""
Below each replica is added to the main graph.

In doing this, we need to relabel the vertices and connect the main graph's hubs to 
its neighbors in the replica.
"""
# Adding G1
mapping = {}
for j in range(250):
    mapping[j] = j + 250 * 1
G1 = nx.relabel_nodes(G1, mapping)
G = nx.compose(G, G1)
for hub in hubs:
    for neighbor in _G.neighbors(hub):
        G.add_edge(hub, neighbor + 250 * 1)

# Adding G2
mapping = {}
for j in range(250):
    mapping[j] = j + 250 * 2
G2 = nx.relabel_nodes(G2, mapping)
G = nx.compose(G, G2)
for hub in hubs:
    for neighbor in _G.neighbors(hub):
        G.add_edge(hub, neighbor + 250 * 2)

# Adding G3
mapping = {}
for j in range(250):
    mapping[j] = j + 250 * 3
G3 = nx.relabel_nodes(G3, mapping)
G = nx.compose(G, G3)
for hub in hubs:
    for neighbor in _G.neighbors(hub):
        G.add_edge(hub, neighbor + 250 * 3)

# Adding G4
mapping = {}
for j in range(250):
    mapping[j] = j + 250 * 4
G4 = nx.relabel_nodes(G4, mapping)
G = nx.compose(G, G4)
for hub in hubs:
    for neighbor in _G.neighbors(hub):
        G.add_edge(hub, neighbor + 250 * 4)

# If all is well, this should have 1250 nodes
print(G)



"""
Below a plot is created to visualize the hierarchical benchmark graph.

We shift all replicas to the north, east, south, and west of the main graph.
"""
coors = nx.spring_layout(_G, seed=SEED)
_coors = deepcopy(coors)
for key in _coors:
    coors[key + 250 * 1] = deepcopy(coors[key])
    coors[key + 250 * 1][0] += 1
    
    coors[key + 250 * 2] = deepcopy(coors[key])
    coors[key + 250 * 2][1] -= 1
    
    coors[key + 250 * 3] = deepcopy(coors[key])
    coors[key + 250 * 3][0] -= 1
    
    coors[key + 250 * 4] = deepcopy(coors[key])
    coors[key + 250 * 4][1] += 1

plt.figure(figsize=(10, 8))
plt.axis("off")
nx.draw_networkx_nodes(G, coors, nodelist=list(range(0, 250)), node_size=5, node_color="#00BFFF")
nx.draw_networkx_nodes(G, coors, nodelist=list(range(250, 500)), node_size=5, node_color="#228B22")
nx.draw_networkx_nodes(G, coors, nodelist=list(range(500, 750)), node_size=5, node_color="#DAA520")
nx.draw_networkx_nodes(G, coors, nodelist=list(range(750, 1000)), node_size=5, node_color="#9932CC")
nx.draw_networkx_nodes(G, coors, nodelist=list(range(1000, 1250)), node_size=5, node_color="#F4A460")
nx.draw_networkx_edges(G, coors, alpha=0.1)
#plt.show()
plt.savefig("figures/misc/rb_lfr_example.png", bbox_inches="tight")