SEED = 10

import networkx as nx                 # Network uttilities
import matplotlib.pyplot as plt       # Plotting
import numpy as np                    # Always useful
from scipy.cluster import hierarchy   # Plotting dendrograms, linkage methods, etc.
from scipy.spatial import distance    # Computing all-pairs shortest paths
from sklearn import metrics           # Silhouette score dependency
import pandas as pd                   # For using dataframes
from collections import defaultdict   # Useful datastructure



"""
This class is where all the magic happens. It accepts variations of the 
Ravasz and Girvan-Newman algorithms as input. Then it automatically creates 
clusters for each algorithm, using the silhouette score. These clusters are 
used to create hypercluster, which are converted to a pairwise co-occurrence 
matrix. This pairwise co-occurrence matrix is used as input for an agglomerative 
algorithm, to create the final dendrogram.
"""
class Ensemble:
    def __init__(self, label: str, data, algorithms: dict[list], max_clusters: int):
        self.label = label
        self.data = data
        self.n_nodes = len(self.data)
        self.algorithms = algorithms
        self.max_clusters = max_clusters
        self.consensus_method = consensus_method
        self.distances = self.create_distance_matrix()
        self.dendrograms = {}
        self.clusterings = {}
        self.silhouette_scores = pd.DataFrame(columns=["algorithm", "n_clusters", "score"])
        self.co_occurrences = None
        self.dendrogram = None

    
    """
    Run all algorithms in the ensemble, and create a single dendrogram.
    """
    def run(self, consensus_method: str):
        # Find clusterings
        for method in self.algorithms.keys():
            for linkage in self.algorithms[method]:
                if method == "agglomerative":
                    clustering, dendrogram = self.agglomerative(linkage)
                elif method == "divisive":
                    clustering, dendrogram = self.divisive(linkage)
                else:
                    raise ValueError
                self.clusterings[f"{method}-{linkage}"] = clustering
                self.dendrograms[f"{method}-{linkage}"] = dendrogram
                
        # Create hyperclusters
        self.create_co_occurrence_matrix()
        self.find_consensus(consensus_method)
        
    
    """
    Create a distance matrix using all pairs shortest path between vertices.
    """
    def create_distance_matrix(self):
        path_length = nx.all_pairs_shortest_path_length(self.data)
        distances = np.zeros((self.n_nodes, self.n_nodes))
        for node, info in path_length:
            for other_node, l in info.items():
                distances[node][other_node] = l
                distances[other_node][node] = l
                if node == other_node:
                    distances[node][node] = 0
        return distances
        
    
    """
    Agglomerative clustering (Ravasz).
    """
    def agglomerative(self, linkage: str):
        condensed_distances = distance.squareform(self.distances)
        dendrogram = hierarchy.linkage(condensed_distances, method=linkage)
        best_cut = self.find_best_cut(dendrogram, algorithm="agglomerative", linkage=linkage)
        clustering = hierarchy.fcluster(dendrogram, t=best_cut, criterion="maxclust")
        return clustering, dendrogram
    
    
    """
    Divisive clustering (Girvan-Newman).
    """
    def divisive(self, linkage: str):
        # Create the linkage function
        def linkage_function(G):
            if linkage == "betweenness":
                centrality = nx.edge_betweenness_centrality(G)
            elif linkage == "load":
                centrality = nx.edge_load_centrality(G)
            return max(centrality, key=centrality.get)
        
        # Cast iterator type to list for convenience
        dendrogram = list(nx.community.girvan_newman(self.data, most_valuable_edge=linkage_function))
        best_cut = self.find_best_cut(dendrogram, algorithm="divisive", linkage=linkage)
        clustering = self.convert_girvan_newman_clustering(dendrogram[best_cut - 1])
        return clustering, dendrogram
    
    
    """
    Helper function to convert a list of clusters to a single list of assignments.
    """
    def convert_girvan_newman_clustering(self, clustering):
        cluster_assignments = [None] * len(self.data)
        i = 1
        for cluster in list(clustering):
            for node in cluster:
                cluster_assignments[node] = i
            i += 1
        return cluster_assignments
    
    
    """
    Find optimal number of clusters for agglomerative procedurs, using silhouette score.
    """
    def find_best_cut(self, clusterings, algorithm, linkage):
        best_cut = 1
        best_score = -1
        for i in range(2, self.max_clusters):
            try:
                # Cut the number of clusters
                if algorithm == "agglomerative":
                    nodes = hierarchy.fcluster(clusterings, t=i, criterion="maxclust")
                elif algorithm == "divisive":
                    nodes = self.convert_girvan_newman_clustering(clusterings[i - 1])
                else:
                    raise ValueError
                
                score = metrics.silhouette_score(self.distances, nodes, metric="euclidean")
                if score > best_score:
                    best_cut = i
                    best_score = score
                    
                # Store silhouette scores for output analysis
                self.silhouette_scores.loc[len(self.silhouette_scores.index)] = [f"{algorithm}-{linkage}", i, score] 
            except Exception as e:
                print(e)
        
        return best_cut
    
    
    """
    Go through all clusterings and store the pairwise co occurence.
    """
    def create_co_occurrence_matrix(self):
        max_co_occurrences = len(algorithms["agglomerative"]) + len(algorithms["divisive"])
        self.co_occurrences = np.full((self.n_nodes, self.n_nodes), max_co_occurrences)
        for clustering in self.clusterings.values():
            for i in range(self.n_nodes):
                for j in range(i, self.n_nodes):
                    if clustering[i] == clustering[j]:
                        self.co_occurrences[i][j] -= 1
                        self.co_occurrences[j][i] -= 1
                        if i == j:
                            self.co_occurrences[i][j] = 0
                            
    
    """
    Final method in the ensemble approach to create a single dendrogram.
    """
    def find_consensus(self, consensus_method):
        condensed_distances = distance.squareform(self.co_occurrences)
        dendrogram = hierarchy.linkage(condensed_distances, method=consensus_method)
        self.dendrogram = dendrogram
        
    
    """
    Converts dendrogram to a dataframe containing the clusters.
    """
    def to_clusters(self, max_clusters: int):
        clustering = hierarchy.fcluster(self.dendrogram, t=max_clusters, criterion="maxclust")
        df_clusters = pd.DataFrame(columns=["node", "cluster"])
        for i in range(len(clustering)):
            df_clusters.loc[i] = [i + 1, clustering[i]]
        return df_clusters



"""
This is a helper method to obtain a number of clusters from all the 
clusterings the Girvan Newman algorithm outputs.
"""
def fcluster_girvan_newman(n_nodes, clusterings, t: int):
    cluster_assignments = [None] * n_nodes
    i = 1
    for cluster in list(clusterings[t-1]):
        for node in cluster:
            cluster_assignments[node] = i
        i += 1
    return cluster_assignments



"""
Experiment 1:

We run the first experiment on the Karate Club graph. A total of 8 
algorithms are used in the ensemble.
"""
G = nx.karate_club_graph()
n_nodes = len(G)

# Add ground truth clustering
mrhi = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 16, 17, 19, 21]
johna = [9, 14, 15, 18, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33]
for node in G.nodes():
    if node in mrhi:
        G.nodes()[node]["group"] = "mrhi"
    else:
        G.nodes()[node]["group"] = "johna"

# Fix a structure for the vertices
coors = nx.spring_layout(G, seed=SEED)

# Visualize the network
plt.figure(figsize=(10, 8))
plt.axis("off")
nx.draw_networkx_nodes(G, coors, nodelist=mrhi, node_size=500, node_color="#d8031c")
nx.draw_networkx_nodes(G, coors, nodelist=johna, node_size=500, node_color="#6497bf")
nx.draw_networkx_edges(G, coors)
nx.draw_networkx_labels(G, coors, font_color="white")
plt.savefig("figures/karate/network_karate.png", bbox_inches="tight")

# Create the ensemble with average linkage to obtain a consensus
max_clusters = 10
consensus_method = "average"
algorithms = {
    "agglomerative": ["complete", "average", "weighted", "centroid", "median", "ward"],
    "divisive": ["betweenness", "load"]
}
ensemble = Ensemble(label="ensemble", data=G, algorithms=algorithms, max_clusters=max_clusters)
ensemble.run(consensus_method=consensus_method)

# Save the silhouette scores for further analysis in R
ensemble.silhouette_scores.to_csv("data/results/silhouette_scores_karate.csv", index=False)

# Obtain normalized mutual information (NMI) scores
df_nmi = pd.DataFrame(columns=["algorithm", "nmi"])

# Put the ground truth in the same data structure as the clusterings
ground_truth = [None] * n_nodes
for i in range(len(mrhi)):
    ground_truth[mrhi[i]] = 1
for i in range(len(johna)):
    ground_truth[johna[i]] = 2

# Store NMI scores for ensemble
clustering = hierarchy.fcluster(ensemble.dendrogram, t=2, criterion="maxclust")
nmi = metrics.normalized_mutual_info_score(ground_truth, clustering)
df_nmi.loc[len(df_nmi.index)] = ["ensemble", nmi]

# Store NMI scores for each algorithm in the ensemble
for algorithm, _ in ensemble.clusterings.items():
    if algorithm.startswith("agglomerative"):
        clustering = hierarchy.fcluster(
            ensemble.dendrograms[algorithm], 
            t=2, 
            criterion="maxclust"
        )
    elif algorithm.startswith("divisive"):
        clustering = fcluster_girvan_newman(
            n_nodes, 
            ensemble.dendrograms[algorithm],
            t=2
        )
    nmi = metrics.normalized_mutual_info_score(ground_truth, clustering)
    df_nmi.loc[len(df_nmi.index)] = [algorithm, nmi]

# Save the NMI scores for further analysis in R
df_nmi.to_csv("data/results/nmi_karate.csv", index=False)

# Create a figure for the final dendrogram
plt.figure(figsize=(10, 8))
hierarchy.dendrogram(ensemble.dendrogram)
plt.savefig("figures/karate/ensemble_dendrogram_karate.png", bbox_inches="tight")



"""
Experiment 2:

We run another experiment on the Americal College Footbal network. 
Again, a total of 8 algorithms are used in the ensemble.
"""
G = nx.read_gml("data/networks/football/football.gml")
n_nodes = len(G)

# Map all node labels to integers, otherwise we cannot run the algorithms
mapping = {}
nodes = list(G.nodes)
for i in range(len(nodes)):
    mapping[nodes[i]] = i
G = nx.relabel_nodes(G, mapping)

# Node lists are used in visualizing the network (coloring of nodes)
node_lists = defaultdict(list)
for i in range(len(G.nodes)):
    group = G.nodes[i]["value"]
    node_lists[group].append(i)

# Each ground-truth cluster needs a color for visualization
color_palette = [
    "#DC143C",
    "#00BFFF",
    "#228B22",
    "#DAA520",
    "#9932CC",
    "#F4A460",
    "#4682B4",
    "#FF6347",
    "#3CB371",
    "#6A5ACD",
    "#FF7F50",
    "#BDB76B"
]

# Fix the coordinates of all nodes
coors = nx.spring_layout(G, seed=SEED)

# Create a figure for the network
plt.figure(figsize=(10, 8))
plt.axis("off")
for i in range(len(node_lists)):
    nx.draw_networkx_nodes(G, coors, nodelist=node_lists[i], node_size=500, node_color=color_palette[i])
nx.draw_networkx_edges(G, coors, alpha=0.5)
nx.draw_networkx_labels(G, coors, font_color="white")
plt.savefig("figures/football/network_football.png", bbox_inches="tight")

# Create an ensemble
max_clusters = 20
consensus_method = "average"
algorithms = {
    "agglomerative": ["complete", "average", "weighted", "centroid", "median", "ward"],
    "divisive": ["betweenness", "load"]
}
ensemble = Ensemble(label="ensemble", data=G, algorithms=algorithms, max_clusters=20)
ensemble.run(consensus_method=consensus_method)

# Save the silhouette scores for further analysis in R
ensemble.silhouette_scores.to_csv("data/results/silhouette_scores_football.csv", index=False)

# Save the dendrogram of the ensemble
plt.figure(figsize=(10, 8))
hierarchy.dendrogram(ensemble.dendrogram)
plt.savefig("figures/football/ensemble_dendrogram_football.png", bbox_inches="tight")

# Create an empty dataframe for storing NMI scores 
df_nmi = pd.DataFrame(columns=["algorithm", "nmi"])

# Put the ground truth in the same data structure as the clusterings
ground_truth = []
for i in range(len(G)):
    ground_truth.append(G.nodes[i]["value"])

# Obtain NMI score for ensemble
clustering = hierarchy.fcluster(ensemble.dendrogram, t=12, criterion="maxclust")
nmi = metrics.normalized_mutual_info_score(ground_truth, clustering)
df_nmi.loc[len(df_nmi.index)] = ["ensemble", nmi]

# Obtain NMI scorse for each algorithm in the ensemble
for algorithm, _ in ensemble.clusterings.items():
    if algorithm.startswith("agglomerative"):
        clustering = hierarchy.fcluster(
            ensemble.dendrograms[algorithm], 
            t=12, 
            criterion="maxclust"
        )
    elif algorithm.startswith("divisive"):
        clustering = fcluster_girvan_newman(
            n_nodes, 
            ensemble.dendrograms[algorithm],
            t=12
        )
    nmi = metrics.normalized_mutual_info_score(ground_truth, clustering)
    df_nmi.loc[len(df_nmi.index)] = [algorithm, nmi]
    
# Save all NMI scores for further analysis in R
df_nmi.to_csv("data/results/nmi_football.csv", index=False)



# This creates an example figure of a dendrogram (not relevant)
plt.figure(figsize=(10, 8))
hierarchy.dendrogram(ensemble.dendrograms["agglomerative-median"], no_labels=True)
plt.xticks([])
plt.yticks([])
plt.box(False)
plt.savefig("figures/misc/example_dendrogram.png", bbox_inches="tight")