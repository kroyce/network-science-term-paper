library(tidyverse)

# Karate Club -------------------------------------------------------------

silhouette_scores_karate <- read_csv(
  "../data/results/silhouette_scores_karate.csv",
  col_types = list(col_factor(), col_integer(), col_double()))
silhouette_scores_karate %>%
  ggplot(aes(x = n_clusters, y = score, color = algorithm, group = algorithm)) +
  geom_line() +
  geom_point() +
  geom_vline(aes(xintercept = 2), linetype="dashed") +
  scale_y_continuous(limits = c(-1, 1)) +
  theme_minimal() +
  labs(x = "Number of Clusters", y = "Silhouette Score")
ggsave("../figures/karate/silhouette_scores_karate.png", width = 6, height = 3)

nmi_karate <- read_csv(
  "../data/results/nmi_karate.csv", 
  col_types = list(col_factor(), col_double()))
nmi_karate %>%
  ggplot(aes(x = algorithm, y = nmi)) +
  geom_col() +
  coord_flip() +
  scale_y_continuous(limits = c(0, 1)) +
  theme_minimal() +
  labs(x = NULL, y = "Normalized Mutual Information")
ggsave("../figures/karate/nmi_karate.png", width = 6, height = 3)


# American College Football -----------------------------------------------

silhouette_scores_football <- read_csv(
  "../data/results/silhouette_scores_football.csv",
  col_types = list(col_factor(), col_integer(), col_double()))
silhouette_scores_football %>%
  ggplot(aes(x = n_clusters, y = score, color = algorithm, group = algorithm)) +
  geom_line() +
  geom_point() +
  geom_vline(aes(xintercept = 12), linetype="dashed") +
  scale_y_continuous(limits = c(-1, 1)) +
  theme_minimal() +
  labs(x = "Number of Clusters", y = "Silhouette Score")
ggsave("../figures/football/silhouette_scores_football.png", width = 6, height = 3)
silhouette_scores_football %>%
  ggplot(aes(x = n_clusters, y = score, color = algorithm, group = algorithm)) +
  geom_line() +
  geom_point() +
  geom_vline(aes(xintercept = 12), linetype="dashed") +
  theme_minimal() +
  labs(x = "Number of Clusters", y = "Silhouette Score")
ggsave("../figures/football/silhouette_scores_football_zoomed.png", width = 6, height = 3)

silhouette_scores_football %>%
  group_by(algorithm) %>%
  filter(score == max(score)) %>%
  xtable::xtable() %>%
  print(include.rownames=FALSE)

nmi_football <- read_csv(
  "../data/results/nmi_football.csv", 
  col_types = list(col_factor(), col_double()))
nmi_football %>%
  ggplot(aes(x = algorithm, y = nmi)) +
  geom_col() +
  coord_flip() +
  scale_y_continuous(limits = c(0, 1)) +
  theme_minimal() +
  labs(x = NULL, y = "Normalized Mutual Information")
ggsave("../figures/football/nmi_football.png", width = 6, height = 3)

nmi_football %>%
  xtable::xtable(digits=4) %>%
  print(include.rownames=FALSE)
