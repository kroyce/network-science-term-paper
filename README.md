# Implementation
The implementation was made in Python (version 3.9). To set up, create a virtual environment and install the dependencies as follows:
```sh
cd src
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

Then run the code:
```sh
python script.py
```

For reproducibility, a seed is set to $10$. All data is stored in the folder `data/results`.

## Jupyter Notebook
We also used a Jupyter notebook (`results.ipynb`), which does exactly the same as `script.py`. Make sure to set the kernel to the created environment when using this notebook.

## RB-LFR Benchmark Graphs
The authors of the RB-LFR did not share code for generating RB-LFR benchmark graphs. In the file `RB_LFR.py`, a minimal setup for generating these benchmark graphs is given. Further research could use these benchmark graphs to analyze the ensemble approach.

# Analysis
The plots in the paper were made using the R programming language. For this you only need the `tidyverse` package, simply run `install.packages("tidyverse")`. However, all plots are already generated, and can be found in the folder `figures`. The script itself is located at: `analysis/main.R`.

Using this R script, all tables are also automatically generated, thanks to the useful `xtable` package. This package can be installed similarly: `install.packages("xtable")`.

# Data
All input data can be found in `data/networks`. All output data can be found in `data/results`.

# Figures
The figures that were used in the term paper are located in the `figures` folder. Both networks analyzed have their own folder (`figures/karate`, and `figures/football`).